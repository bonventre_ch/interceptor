
'''
- Create the new TagComplete (oder so)
Ziel: Das Ziel des Programmes ist sicherzustellen dass alle Tags die heute bei OFA gefüllt werden auch von uns gefüllt sind.
Wird vor allem genutzt für Bestelllungen von OF: Wir haben die Originalbestellung via Interceptor und eine Bestellung von D365. Inhalte selbstverständlich nicht gleich aber die gleiche Anzahl Tags sollte gefüllt sein.

basisFile / toCompareFile
für jeden Tag inhalt im basisFile schaut ob in toCompareFile der Tag auch gefüllt ist
 <Sender>Lieferanten direkt</Sender> -> der Tag Sender ist gefüllt
 Weil die Tags mehrmals kommen, solltest du den Pfad sicherstellen.
 <?xml version="1.0" encoding="UTF-8"?>
<i2Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Header>
    <Sender>Lieferanten direkt</Sender>
    <Recipient>Siemens</Recipient>
    <MessageType>Order.OUT</MessageType>
    <MessageParameter>iXMLOriginal</MessageParameter>
    <CreationDate>2021-03-09T07:17:22.159Z</CreationDate>
    <Version>1.7</Version>

 
Der Tag <Header><Version> ist gefüllt
Das Sytem sollte nicht nur die Tag Inhalte sonden auch die attribute

Beispiel: Im Tag <i2Document> ist der Attribut xmlns:xsi gefüllt

Erwarteter Output:
In File ist Tag <ccc><ddd><eee> nicht gefüllt. Im original File: <ccc><ddd><eee>Wert
In File ist Attribut <ccc><ddd><eee attibutname> nicht gefüllt. Im original File: <ccc><ddd><eee attributname=wert>

<xml>
	<document>
		<type>
			order
		</type>
		<header>
			<adress>
				<street>test</street>
				<nr>123</nr>
				<type></type>
			</adress>
		</header>
	</document>

</xml>

'''

import sys
import os
import pathlib
import shutil
import logging
import json
import time
from datetime import datetime, timedelta
import pysftp
import smtplib
from email.message import EmailMessage
from decouple import Config, RepositoryEnv

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import common
from common import join_str_path, path_has_files, init_logger, construct_msg
import xml.etree.ElementTree as ElementTree

# wo läuft das Skript und vo da die .env laden
working_path = os.path.abspath(os.path.split(sys.argv[0])[0])
working_path += "/.env"
env_config = Config(RepositoryEnv(working_path))


def check_config():
	success = False
	try:
		config_entrys = ["LOG_LEVEL"]
		for entry in config_entrys:
			env_config(entry)
		success = True
	except:
		main_logger.error(f"[main] {entry} not all .env values have been provided")
	finally:
		return success

def get_element_path(parent, path=""):
	for child in parent.iter(None):
		path = path + "/" + child.tag
	return path

def add_to_list(element_path, paths):
	if element_path == "":
		return paths
	if not element_path in paths:
		paths.append(element_path)
	return paths

def main():
    # print(sys.argv[1:])

	with open("comparer/test1.xml") as f1:
		tree = ElementTree.parse(f1)
	
	root = tree.getroot()
	xml_str = ElementTree.tostring(root, encoding="unicode")

	with open("comparer/test2.xml") as f2:
		tree2 = ElementTree.parse(f2)

	root2 = tree2.getroot()
	# xml_str2 = ElementTree.tostring(root2, encoding="unicode")

	for child in root:
		print(child.tag)
	
	xml_str = xml_str.replace("\n", "")
	xml_str = xml_str.replace("  ", " ")
	xml_str = xml_str.replace("  ", " ")
	xml_str = xml_str.replace(" ", "")
	
	tags = xml_str.split(">")
	path = ""
	paths = []

	for tag in tags:

		if tag == "":
			continue

		# removes withespace at begining and end
		tag = tag.strip() 
		tag = tag.replace("<", "")
		if tag[-1] == "/":
			tag = tag[0:(len(tag)-1)]
		
		#Anfangs tag
		if (tag.find("/") == -1):
			# anfangs Tag
			path = path + "/" + tag
			paths = add_to_list(path, paths)
		
		# Schliessender Tag
		if (tag.find("/") == 0):
			# split tag from path and everything after
			split_path = path.split(tag)
			paths = add_to_list(split_path[0], paths)
		
		# Selbstschliessendr Tag
		if (tag.find("/") > 0):
			paths = add_to_list((path + "/" + tag), paths)

		attributes = {}
		# remove the root element from the search string
		last_item = paths[-1]
		search_string = last_item.split(paths[0])[1]

		# check if element has attributes
		if not len(paths) == 1:
			current_element = root.findall(f"./{search_string}")
			attributes = current_element[0].attrib

			# check path in file to compare
			element_to_compare = root2.findall(f"./{search_string}")
			attributes_to_compare = current_element[0].attrib

			# compare value and attribute

		
	print(paths)


		# <kkjkjjk> </hjjhjhhj> <jhjjhjhjhjjh />

		# wenn wir splitten <kkjkjjk </hjjhjhhj <jhjjhjhjhjjh /
		# das heisst wenn im string kein / gitb -> ist einen anfangs tag
		# wenn im string einen / am anfang :-> es ist einen endtag 
		# wenn im string einen / am ende :-> es ist einen endtag 

		# was ist der Unterschied zwishcen die 2 endtags?
		# wenn endtag / am anfang ist muss ich mein "path" im Baum anpassen
		# wenn endtag / am ende ist muss ich einmal printen aber der pfad im baum ändert sich nicht
# <OrderRequest>
#    <OrderRequestHeader>
#       <OrderRequestNumber>
#          <BuyerOrderRequestNumber/>
#       </OrderRequestNumber>
#       <OrderRequestIssueDate>T</OrderRequestIssueDate>
#       <Purpose>
#          <PurposeCoded>InformationOnly</PurposeCoded>
#       </Purpose>
#       <RequestedResponse>
#          <RequestedResponseCoded>NoAcknowledgementNeeded</RequestedResponseCoded>
#       </RequestedResponse>
#       <OrderRequestCurrency>
#          <Currency>
#             <CurrencyCoded/>
#          </Currency>
#       </OrderRequestCurrency>
#       <OrderRequestLanguage>

# <OrderRequest>
# <OrderRequest><OrderRequestHeader>
# <OrderRequest><OrderRequestHeader><OrderRequestNumber>
# <OrderRequest><OrderRequestHeader><OrderRequestNumber><BuyerOrderRequestNumber/>
# <OrderRequest><OrderRequestHeader>

# OrderRequest number=ertz place=34		
# suchen leerschlag und alles was links vom leerschlag ist nehmen
# spliten nach leerschschalg und 0 nehmen 


	# 		if tag[0] == "/":
	# 			tag = tag.replace("/", "")
			
	# 		# remove tailing slash
	# 		'''
	# 		if tag[-1] == "/":
	# 			tag = tag.replace("/", "")
	# 		'''

	# 		# closing tag
	# 		if tag[-2] == "/":
	# 			# hier muss der tag von path entfernen
	# 			continue

	# 		# self closing tag
	# 		if tag[1] == "/":
	# 			check_element(path + tag)
	# 			continue

	# 		# 
	# 		path = path + "/" + tag.replace("<", "")
	# 		check_element(path)
		
	# 	'''
	# 	opening = xml_str.find("<")
	# 	closing = xml_str.find(">")
		
	# 	slice_object = slice(opening + 1, closing) # plus one???
	# 	x = xml_str[slice_object]

	# 	# remove everything before the closing point and repeat

	# 	print("Done")
	# 	'''
	# '''
	# with open("file1") as f1:
	# 	tree = ET.parse(f)
	# 	unique_key1 = tree.findall(f"./{entry['tags']}[@{entry['unique_attribute']}]")[0].attrib[f"{entry['unique_attribute']}"]
	# 	file_path1 = x
	# '''

	# '''
	# with open("comparer/8200.xml") as f1:
	# 	tree = ET.parse(f1)
	# 	root = tree.getroot()
	# 	my_list = root.iter(None)
	# 	for child in root.iter(None):
	# 		print(child)
	# 		for child1 in child.iter(None):
	# 			print(child1)
	# 		# print(tree.findall(f".//*..."))
	# 		# print(child)
	# '''

	# '''
	# my_list = []
	# with open("comparer/8200.xml") as f1:
	# 	tree = ET.parse(f1)
	# 	root = tree.getroot()
	# 	my_var = root.iter()
	# 	for child in root.iter(None):
	# 		my_list.append(child.tag)
	# 		for new_child in child:
	# 			my_list.append(new_child.tag)
	# print("done")

	# '''


	# # second file with findall for path search
	# iterfind

		
	# for each tag
	#	get attr list with value
	# F1: <rechnung><emp typ="dead" cost ="123"</emp></rechnung>
	# F2: <rechnung><emp typ="alive" cost ="123"</emp></rechnung>
	# try f2.tag wie in f1 -> fail .... /rechnung/emp/.....
	
    # check if the provided files are valid xml's
    # for each node, create dict -> name, parent, set() of children
    #       check if current node has attributes
    #           set bool in dict has_attr
    #           set 
    #       check if the attr has a value
    #           save value in to tmp
    #       -> check if the node and attribute exists in the second xml

	

main_logger = common.init_logger("main_logger", env_config("LOG_LOCATION_COMPARE_TAGS"), int(env_config("LOG_LEVEL")))
main()
