import sys
import os
import pathlib
from decouple import config
import xml.etree.ElementTree as ET
from common import join_str_path, init_logger

# Todo
# usage output, output file sspezifiscvh, text output benutzerfeundlich


def get_tags(tree):
	tags = set()
	for elm in tree.iter():
		tags.add(elm.tag)
	main_logger.debug(f"[get_tags] {tags}")
	return tags


def create_tree(file_name):
	with open(file_name) as file_content:
		tree = ET.parse(file_content)
	main_logger.debug(f"[create_tree] {tree}")
	return tree


def make_string(elm):
	tag_text = '<' + elm.tag
	if len(elm.attrib) > 0:
		for atr in elm.attrib:
			tag_text = tag_text + " " + atr + f"='{elm.attrib[atr]}'"
	tag_text = tag_text + ">"
	if not elm.text == None:
		text = elm.text.strip()	
		if len(text) > 0:
			tag_text = tag_text + elm.text
	main_logger.debug(f"[make_string] {tag_text}")
	return tag_text


def list_differences(differences):
	for e in differences:
		print(e)


def has_differences(a, b):
	main_logger.debug(f"[has_differences] file_1 hat {len(a)} elemente")
	main_logger.debug(f"[has_differences] file_2 hat {len(b)} elemente")
	# in a aber nicht in b
	result = a.difference(b)
	if len(result) > 0:
		main_logger.debug(f"[has_differences] folgende elemente fehlen: ")
		list_differences(result)
		return True
	else:
		main_logger.debug(f"[has_differences] alle file_1 tags sind in file_2")

	result = b.difference(a)
	if len(result) > 0:
		main_logger.debug(f"[has_differences] folgende elemente existieren in file_2 aber nicht in file_1: ")
		list_differences(result)
	else:
		main_logger.debug(f"[has_differences] die tags in file_2 sind gleich wie in file_1")
		return False
	return True


def main():
	main_logger.debug(f"[main] code execution started")

	orig_tree = create_tree(file1)
	orig_tags = get_tags(orig_tree)

	test_tree = create_tree(file2)
	test_tags = get_tags(test_tree)

	if (has_differences(orig_tags, test_tags) == True):
		main_logger.info(f"[main] files sind nicht gleich, stopping")
		raise SystemExit(1)

	orig = []
	for elm in orig_tree.iter():
		orig.append(make_string(elm))

	test = []
	for elm in test_tree.iter():
		test.append(make_string(elm))

	for e in orig:
		if e in test:
			test.remove(e)
		else:
			main_logger.debug(f"[main] von file_1: '{e}' nicht vorhanden in file_2")

	for e in test:
		if e in orig:
			orig.remove(e)
		else:
			main_logger.debug(f"[main] von file_2: '{e}' nicht vorhanden in file_1")


main_logger = init_logger("main_logger", config("LOG_LOCATION_STAGING"), int(config("LOG_LEVEL")))
file1 = ""
file2 = ""
if __name__ == "__main__":
	file1 = sys.argv[1]
	file2 = sys.argv[2]
	main()
