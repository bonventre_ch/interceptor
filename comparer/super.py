import sys
import os
import pathlib
import json
import shutil
from decouple import config
import xml.etree.ElementTree as ET
from common import join_str_path, init_logger, construct_msg
from compare_elements import make_string, has_differences, get_tags, create_tree

'''
config mit:
	verantwortlicher, unique_key, 

	log pro Verantwortlicher

	{
		"Key" : {
			"Tags" : "Request/InvoiceDetailRequest/InvoiceDetailRequestHeader",
			"Attribute" : "invoiceID"
		}
	}

	root.findall(".//year/..[@invoiceID]")

cli arg: dir mit zwei sub_dir
	für das erste file aus sub_dir_1
		unique_key überprüfen
			{Hier ist unklar wie das zweite File gefunden wird}
			mit file für file aus sub_dir_2 vergleichen
				bei Treffer, in ein separates dir moven nicht kopieren

was passiert mit den erfolgreich verglichenen files? Mail an Verantworticher?
was passiert mit den anderen? -> mail an verantwortlicher aus der config

liste mit Elemente und Werten aus dem ersten File, vergleichen mit den Tags aus dem Zweiten File um zu sehen ob die Files gleich sind, für alle Files aus dem zweiten Ordner
'''

# ToDo:
# log per person
# feller files

def log_for_person(person, txt):
	log_file_name = "super_log_" + person + ".log"
	with open(log_file_name, "a") as compare_log:
		compare_log.write(txt)


def compare(file_path1, file_path2):

	orig_tree = create_tree(file_path1)
	orig_tags = get_tags(orig_tree)

	test_tree = create_tree(file_path2)
	test_tags = get_tags(test_tree)

	if (has_differences(orig_tags, test_tags) == True):
		main_logger.info(f"[compare_files] files sind nicht gleich, stopping")
		return [False, f"[compare_files] files sind nicht gleich, stopping"]

	orig = []
	for elm in orig_tree.iter():
		orig.append(make_string(elm))

	test = []
	for elm in test_tree.iter():
		test.append(make_string(elm))

	for e in orig:
		if e in test:
			test.remove(e)
		else:
			main_logger.debug(f"[compare_files] von file_1: '{e}' nicht vorhanden in file_2")
			return [False, f"[compare_files] von file_1: '{e}' nicht vorhanden in file_2"]

	for e in test:
		if e in orig:
			orig.remove(e)
		else:
			main_logger.debug(f"[compare_files] von file_2: '{e}' nicht vorhanden in file_1")
			return [False, f"[compare_files] von file_2: '{e}' nicht vorhanden in file_1"]

	return [True, f"[compare_files] files sind gleich"]


def move_files(files_to_move, destination):
	for f in files_to_move:
		try:
			shutil.copy2(f, destination)
			os.remove(f)
			# shutil.move(str(f), destination)
		except:
			print("copy failed")

def main():
	super_config = {}
	try:
		with open(config("SUPER_CONFIG_PATH"), "r") as JSON:
			main_logger.debug(f"[main] got file at '{config('SUPER_CONFIG_PATH')}'")
			super_config = json.load(JSON)
	except json.decoder.JSONDecodeError as e:
		main_logger.error(f"[main] could not decode json: {e}")
		raise SystemExit(1)
	except:
		main_logger.error(f"[main] open config and json decode, unexpected error: {sys.exc_info()[0]}")
		raise SystemExit(1)

	unique_key1 = ""
	unique_key2 = ""
	file_path1 = ""
	file_path2 = ""
	log_output = construct_msg("", f"Logger started")
	for entry in super_config["root"]:
		log_output = construct_msg(log_output, f"logging for: '{entry['verantwortlich']}'")
		# goto restarting iter over first path, how?
		for x in pathlib.Path(entry["compare_1"]).iterdir():
			if x.is_file():
				with open(x) as f:
					tree = ET.parse(f)
					unique_key1 = tree.findall(f"./{entry['tags']}[@{entry['unique_attribute']}]")[0].attrib[f"{entry['unique_attribute']}"]
					file_path1 = x
				
				for y in pathlib.Path(entry["compare_2"]).iterdir():
					if y.is_file():
						with open(y) as f:
							tree = ET.parse(f)
							unique_key2 = tree.findall(f"./{entry['tags']}[@{entry['unique_attribute']}]")[0].attrib[f"{entry['unique_attribute']}"]
							file_path2 = y

			if unique_key1 == unique_key2:
				compare_result = compare(file_path1, file_path2)
				if compare_result[0] == False:
					# files not same, next pls
					log_output = construct_msg(log_output, f"{compare_result[1]}")
					log_output = construct_msg(log_output, f"files: '{file_path1}' and {file_path2} don't have the same entries, next pls")
					continue
				else:
					log_output = construct_msg(log_output, f"{compare_result[1]}")
					# don't move to the same dir, use same dir name as input (input_dir_name)_dir_name
					move_files([file_path1, file_path2], config("SUPER_PROCESSED_PATH"))
					main_logger.debug(f"[compare_files] moved files")
			log_for_person(entry['verantwortlich'], construct_msg(log_output, f"finished"))

			'''
			if not compare_was_successful:
				main_logger.debug(f"[compare_files] not same same, exit")
				raise SystemExit(1)
			
			# move after compare_files
			if unique_key1 == unique_key2:
				move_files([file_path1, file_path2], config("SUPER_PROCESSED_PATH"))
				main_logger.debug(f"[compare_files] moved files")
				break
			else:
				continue
			'''

	# logger per verantwortlicher
	
	main_logger.debug(f"[main] finished")

'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
main_logger = init_logger("main_logger", config("LOG_LOCATION_SUPER"), int(config("LOG_LEVEL")))
main()