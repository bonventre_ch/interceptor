import sys
import pathlib
import logging
from random import randint
from decouple import config
import smtplib
from email.message import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import pysftp


def construct_msg(msg_body, new_txt):
	end_of_line = "\n\r"
	return msg_body + end_of_line + new_txt


# unused
def send_msg(msg_body):
	try:
		msg = MIMEMultipart()
		msg["From"] = config("EMAIL_SENDER")
		msg["To"] = config("EMAIL_RECIPIENT")
		msg["Subject"] = config("EMAIL_SUBJECT")
		msg.attach(MIMEText(msg_body))

		mailserver = smtplib.SMTP(config("EMAIL_SERVER"), config("EMAIL_PORT"))
		mailserver.ehlo()
		mailserver.starttls()
		mailserver.login(config("EMAIL_SENDER"), config("EMAIL_PW"))
		mailserver.sendmail(config("EMAIL_SENDER"), config("EMAIL_RECIPIENT"), msg.as_string())
		mailserver.quit()
		return True
	except:
		return False


def init_logger(name, log_file, log_level):
	logging.basicConfig(
		level = log_level,
		format = "%(asctime)s - %(levelname)s - %(message)s",
		handlers = [
			logging.FileHandler(log_file),
			logging.StreamHandler()
		]
	)
	return logging.getLogger(name)


def start_sftp(logger, working_dir, sftp_path):
	try:
		hostname = config("SFTP_HOSTNAME")
		username = config("SFTP_USERNAME")
		password = config("SFTP_PASSWORD")
		cnopts = pysftp.CnOpts()
		cnopts.hostkeys = None

		source_path = pathlib.Path(working_dir)

		# hostkey "+SD6h60sHVTTMwne/1PSvg6kL6H0jexlrywEQmyitGY"
		with pysftp.Connection(host=hostname, username=username, password=password, cnopts=cnopts) as sftp:
			if sftp.exists(sftp_path):
				for current_file in source_path.iterdir():
					if current_file.is_file():
						sftp.put_d(str(source_path), sftp_path, preserve_mtime=True)
	except PermissionError as e:
		logger.error("[start_sftp] PermissionError error({0}): {1}".format(e.errno, e.strerror))
	except OSError as e:
		logger.error("[start_sftp] OSError error({0}): {1}".format(e.errno, e.strerror))
	except:
		logger.error(f"[start_sftp] unexpected error: {sys.exc_info()[0]}")


# unused
def random_list_item(list):
	return list[randint(0, len(list)-1)]


def path_has_files(path_to_check, logger):
	try:
		count = 0
		for input_file in pathlib.Path(path_to_check).iterdir():
			if input_file.is_file():
				count += 1
		if count > 0:
			return True
		logger.info("[path_has_files] no files in sorting input path")
		return False
	except:
		logger.error(f"[path_has_files] unexpected error: {sys.exc_info()[0]}")
		raise SystemExit(1)


# unused
def check_path_exists(path, logger):
	is_valid = False
	try:
		if pathlib.Path(path).is_dir():
			is_valid = True
	except:
		logger.error(f"[check_path_exists] unexpected error: {sys.exc_info()[0]}")
	else:
		logger.debug(f"[check_path_exists] path is valid")
	finally:
		return is_valid		


def join_str_path(base_path, str_to_add):
	return pathlib.Path.joinpath(pathlib.Path(base_path), str_to_add)