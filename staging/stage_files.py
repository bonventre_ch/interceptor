import sys
import os
import pathlib
import shutil
import time
import json
import datetime
from decouple import config
from common import join_str_path, init_logger
import pysftp
import xml.etree.ElementTree as ET


def write_copylog(lines):
	with open(config("LOG_LOCATION_STAGING_STATS"), "a") as copylog_file:
		for line in lines:
			now = datetime.datetime.now()
			copylog_file.write(now.strftime("%d-%m-%Y;%H:%M:%S") + ";" + line + "\n")

	with open(config("STAGING_COPY_PROTECTION_LOG"), "a") as copylog_file:
		for line in lines:
			copylog_file.write('%s\n' % line)

def filter_file(xml_tree, to_filter):
	# filter out non order/invoice documents
	if to_filter in xml_tree._root.tag:
		return True
	return False


def compare_and_copy(customer_input_path, document):
	was_compare_successful = False
	try:
		copylog_lines = []
		try:
			with open(config("STAGING_COPY_PROTECTION_LOG")) as copylog_file:
				copylog_lines = copylog_file.read().splitlines()
		except FileNotFoundError as e:
			main_logger.warning(f"[compare_and_copy] FileNotFoundError error({e.errno}): {e.strerror}")
			with open(config("STAGING_COPY_PROTECTION_LOG"), "w") as copylog_file:
				for line in copylog_lines:
					copylog_file.write("")
		except:
			main_logger.error(f"[compare_and_copy] Unexpected error: {sys.exc_info()[0]}")

		lines = set()
		for line in copylog_lines:
			lines.add(line)

		staging_input = set()
		for x in pathlib.Path(customer_input_path).iterdir():
			if x.is_file():
				if x.suffix in document["docTypes"]:
					if x.suffix == ".xml":
						# es gibt sonic protokolle im xml format die nur intern gebraucht werden, nicht zu kopieren
						try:
							tree = ET.parse(x)
						except ET.ParseError as e:
							main_logger.debug(f"[compare_and_copy] file '{x}' is not a valid xml, skipping")
							continue
						# es gibt auch SOAP-Receipts, diese muessen aus gefiltert werden
						if filter_file(tree, "soap/envelope"):
							continue
					staging_input.add(x)
				else:
					main_logger.debug(f"[compare_and_copy] file '{x}' docType not in docType list'{document['docTypes']}'")

		non_dup_input = []
		for input_file in staging_input:
			if input_file.name not in lines:
				non_dup_input.append(input_file)

		tmp_lines = set()
		for current_file in non_dup_input:
			file_name = current_file.name
			destination = join_str_path(config("STAGING_PATH"), document["id"] + file_name)
			# check if customer is one with third party payment provider -> new entry for customer in json
			# bash: for i in ls *.xml; do echo $i; cat $i | grep \<CompanyName\>Feller >> company_names.txt; done
			try:
				if document["multi_cutomer_dir"] == "True":
					with open(current_file) as f:
						if document["multi_cutomer_dir_search_string"] in f.read():
							main_logger.debug(f"[compare_and_copy] file '{current_file}' has '{document['multi_cutomer_dir_search_string']}' in it!")
							file_to_find = current_file._str[:-8] + ".pdf"
							if pathlib.Path(file_to_find).is_file():
								shutil.copy(file_to_find, join_str_path(config("STAGING_PATH"), document["id"] + current_file.name[:-8] + ".pdf"))

						else:
							main_logger.debug(f"[compare_and_copy] file '{current_file}' does NOT have '{document['multi_cutomer_dir_search_string']}' in it!")
							continue
			except:
				main_logger.debug(f"[compare_and_copy] not a 'multi_cutomer_dir'")
			
			try:
				# overwrite
				shutil.copy(current_file, destination)
				# if current_file in dest, compare
			except FileNotFoundError as e:
				main_logger.error(f"[compare_and_copy] FileNotFoundError error({e.errno}): {e.strerror}, filename: {current_file}")
			except:
				main_logger.error(f"[compare_and_copy] copy, unexpected error: {sys.exc_info()[0]}")
				continue
			
			tmp_lines.add(current_file.name)
			if len(tmp_lines) % int(config("STAGING_COPY_THRESHOLD")) == 0:
				write_copylog(tmp_lines)
				tmp_lines.clear()
			else:
				continue
		
		timestamp_path = str(config("STAGING_PATH")) + "/" + str(config("ALARM_TIMESTAMP_FILENAME"))
		ct = datetime.datetime.now()
		now_timestamp = ct.timestamp()
		with open(timestamp_path, "w") as timestamp_file:
			timestamp_file.write(str(now_timestamp))

		write_copylog(tmp_lines)
		was_compare_successful = True

	except FileNotFoundError as e:
		main_logger.error(f"[compare_and_copy] FileNotFoundError error({0}): {1}".format(e.errno, e.strerror))
	except OSError as e:
		main_logger.error(f"[compare_and_copy] OSError error({0}): {1}".format(e.errno, e.strerror))
	except:
		main_logger.error(f"[compare_and_copy] Unexpected error: {sys.exc_info()[0]}")
	
	return was_compare_successful


def upload_files():
	'''Return True if files got uploaded'''
	was_upload_successful = False
	files_counter = 0

	backup_path = config("BACKUP_PATH")
	source_path = config("STAGING_PATH")
	sftp_path = config("SFTP_PATH")
	sftp_hostname = config("SFTP_HOSTNAME")

	cnopts = pysftp.CnOpts()
	cnopts.hostkeys = None

	try: 
		with pysftp.Connection(host=config("SFTP_HOSTNAME"), username=config("SFTP_USERNAME"), password=config("SFTP_PASSWORD"), cnopts=cnopts) as sftp:
			if not sftp.exists(sftp_path):
				main_logger.error(f"[upload_files] path on remote server does not exists")
				return was_upload_successful
			else: 
				main_logger.debug(f"[upload_files] path on remote server exists")

			for current_file in pathlib.Path(source_path).iterdir():
				if current_file.is_file():
					main_logger.debug(f"[upload_files] got file '{current_file.name}'")
					files_counter += 1
				else:
					continue

				source = join_str_path(source_path, current_file.name)
				destination = sftp_path + current_file.name
				try:
					sftp.put(source, destination, preserve_mtime=True)
				except FileNotFoundError as e:
					main_logger.error(f"[upload_files] FileNotFoundError error({e.errno}): {e.strerror}")
				except:
					main_logger.error(f"[upload_files] failed to get files via sftp")
								
				if len(backup_path):
					shutil.move(source, backup_path)
					main_logger.debug(f"[upload_files] moved file '{source}' to '{backup_path}'")
				else:
					os.remove(source)
					main_logger.debug(f"[upload_files] removed file '{source}'")

				main_logger.info(f"[upload_files] copied file: '{current_file.name}' to remote host: '{sftp_hostname}:{sftp_path}'")
				
		main_logger.debug(f"[upload_files] copied {files_counter} files to remote sftp server")
		was_upload_successful = True
				
	except pysftp.AuthenticationException as e:
		main_logger.error(f"[upload_files] AuthenticationException: {e}")
	except pysftp.SSHException as e:
		main_logger.error(f"[upload_files] SSHException: {e}")
	except:
		main_logger.error(f"[upload_files] connection with credentials failed for an unknown reason")
		main_logger.error(f"[upload_files] credentials: host: '{config('SFTP_HOSTNAME')}', username: '{config('SFTP_USERNAME')}', password: '{config('SFTP_PASSWORD')}', cnopts: '{cnopts}'")
		
	return was_upload_successful


# TODO:
# 	- Email on error
# 	- sftp upload with pub-key

def main():
	main_logger.info(f"[main] code execution started")
	try:
		config_entrys = ["BACKUP_PATH", "STAGING_PATH", "SFTP_PATH", "SFTP_HOSTNAME"]
		for entry in config_entrys:
			config(entry)
	except:
		main_logger.error(f"[main] not all .env values have been provided")

	documents_config = {}

	if not pathlib.Path(config("STAGING_PATH")).is_dir():
		main_logger.error(f"[main] staging path '{config('STAGING_PATH')}' does not exist, stopping execution")
		raise SystemExit(1)
	main_logger.debug(f"[main] staging path '{config('STAGING_PATH')}' exists")
		
	try:
		with open(config("STAGING_CONFIG_PATH"), "r") as JSON:
			main_logger.debug(f"[main] got file at '{config('STAGING_CONFIG_PATH')}'")
			documents_config = json.load(JSON)
	except json.decoder.JSONDecodeError as e:
		main_logger.error(f"[main] could not decode json: {e}")
		raise SystemExit(1)
	except:
		main_logger.error(f"[main] open config and json decode, unexpected error: {sys.exc_info()[0]}")
		raise SystemExit(1)

	for document in documents_config["documents"]:
		customer_input_path = join_str_path(documents_config["StagingInputBasePaths"][document["basePath"]], document["staging_input"])
		if not pathlib.Path(customer_input_path).is_dir():
			main_logger.warning(f"[main] documents source dir '{customer_input_path}' does not exist, skipping")
			continue
		else:
			main_logger.debug(f"[main] documents source dir exists '{customer_input_path}'")
			if not compare_and_copy(customer_input_path, document):
				continue
				
	if not upload_files():
		main_logger.error(f"[main] upload unexpected error: {sys.exc_info()[0]}")
	main_logger.info(f"[main] finished")

'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
main_logger = init_logger("main_logger", config("LOG_LOCATION_STAGING"), int(config("LOG_LEVEL")))
main()
