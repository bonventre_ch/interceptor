import sys
import os
import pathlib
import shutil
import logging
import json
import time
from datetime import datetime, timedelta
import pysftp
import smtplib
import lxml.etree as ET
from email.message import EmailMessage
from decouple import Config, RepositoryEnv

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

import common
from common import join_str_path, path_has_files, init_logger, construct_msg

# wo läuft das Skript und vo da die .env laden
working_path = os.path.abspath(os.path.split(sys.argv[0])[0])
working_path += "/.env"
env_config = Config(RepositoryEnv(working_path))


def send_email(timestamp_from_file):
	email = EmailMessage()
	email["From"] = env_config.get("EMAIL_SENDER")
	email["To"] = ""
	email["Cc"] = ""
	email["Subject"] = f"Interceptor Error: No new files copied from OFAG, since {datetime.fromtimestamp(timestamp_from_file)}!"

	mailserver = smtplib.SMTP(env_config("EMAIL_SERVER"), env_config("EMAIL_PORT"))
	mailserver.ehlo()
	mailserver.starttls()
	mailserver.login(env_config("EMAIL_SENDER"), env_config("EMAIL_PW"))
	# mailserver.send_message(email)

def write_stats(txt):
	if txt == "":
		main_logger.debug(f"[write_stats] no stats to write")
	else: 
		with open(env_config("LOG_LOCATION_DISTIBUTE_STATS"), "a") as stats_file:
			now = datetime.now()
			stats_file.write("\n" + now.strftime("%d-%m-%Y;%H:%M:%S") + ";" + txt)


def check_config():
	success = False
	try:
		config_entrys = ["DISTRIBUTING_CONFIG_PATH", "DISTRIBUTING_INPUT_PATH", "DOC_ID_LENGTH", "LOG_LOCATION_DISTRIBUTING", "LOG_LEVEL", "SFTP_PATH", "SFTP_HOSTNAME", "SFTP_USERNAME", "SFTP_PASSWORD"]
		for entry in config_entrys:
			env_config(entry)
		success = True
	except:
		main_logger.error(f"[main] {entry} not all .env values have been provided")
	finally:
		return success


def check_env():
	setup_successful = False
	try:
		main_logger.debug(f"[check_env] checking path '{env_config('DISTRIBUTING_INPUT_PATH')}' (DISTRIBUTING_INPUT_PATH)")
		if pathlib.Path(env_config("DISTRIBUTING_INPUT_PATH")).is_dir():
			setup_successful = True
			main_logger.debug(f"[check_env] distributing input path exists")
		else:
			raise ValueError
	except ValueError as e:
		main_logger.error(f"[check_env] distributing input path '{env_config('DISTRIBUTING_INPUT_PATH')}' does not exist, stopping execution for error: {e}")
	except:
		main_logger.error(f"[check_env] unexpected error: {sys.exc_info()[0]}")
	
	return setup_successful


def get_sftp_files():
	got_files = False
	if env_config("SFTP_PATH") == "":
		return False
	else:
		sftp_path = env_config("SFTP_PATH")

	cnopts = pysftp.CnOpts()
	cnopts.hostkeys = None
	
	remote_path_items = []
	try:
		with pysftp.Connection(host=env_config("SFTP_HOSTNAME"), username=env_config("SFTP_USERNAME"), password=env_config("SFTP_PASSWORD"), cnopts=cnopts) as sftp:
			if not sftp.exists(sftp_path):
				main_logger.error(f"[get_sftp_files] path on remote server does not exists")
				return False
			
			main_logger.debug(f"[get_sftp_files] path on remote server exists")
			try:
				remote_path_items = sftp.listdir(sftp_path)
			except FileNotFoundError as e:
				main_logger.error(f"[get_sftp_files] FileNotFoundError: path on remote server is empty: {e}")
				return False
			
			if not len(remote_path_items):
				main_logger.error(f"[get_sftp_files] remote_path_items is empty")
				return False

			input_path = env_config("DISTRIBUTING_INPUT_PATH")

			main_logger.info(f"[get_sftp_files] need to fetch {len(remote_path_items)} files")
			for idx, f in enumerate(remote_path_items):
				main_logger.debug(f"[get_sftp_files] {idx}: '{f}'")
			try:
				main_logger.info(f"[get_sftp_files] start fetching files, this might take a while")
				sftp.get_d(sftp_path, input_path, preserve_mtime=True)
			except: 
				main_logger.error(f"[get_sftp_files] failed to get files via sftp: {sys.exc_info()[0]}")
				return False

			file_deletion_counter = 0
			for remote_file in remote_path_items:
				remote_file = sftp_path + remote_file
				try:
					sftp.remove(remote_file)
				except FileNotFoundError as e:
					main_logger.error(f"[get_sftp_files] file not found: '{remote_file}' from: '{sftp_path}' exeption: {e}")
					return False
				except:
					main_logger.error(f"[get_sftp_files] failed remove: '{remote_file}' from: '{sftp_path}' except: {sys.exc_info()[0]}")
					return False
				else:
					file_deletion_counter + 1
					main_logger.debug(f"[get_sftp_files] successfully removed: '{remote_file}' from: '{sftp_path}'")

			for local_file in pathlib.Path(input_path).iterdir():
				pathlib.Path(local_file).unlink()

	except pysftp.AuthenticationException as e:
		main_logger.error(f"[get_sftp_files] Authentication: {e}")
		main_logger.error(f"[get_sftp_files] credentials: host: '{env_config('SFTP_HOSTNAME')}', username: '{env_config('SFTP_USERNAME')}', password: '{env_config('SFTP_PASSWORD')}', cnopts: '{cnopts}'")
		return False
	except:
		main_logger.error(f"[get_sftp_files] failed to get files via sftp, unexpected error: {sys.exc_info()[0]}")
		return False
	
	return True


def get_documents():
	documents = {}
	try:
		with open(env_config("DISTRIBUTING_CONFIG_PATH"), "r") as JSON:
			documents = json.load(JSON)
	except json.decoder.JSONDecodeError as e:
		main_logger.error(f"[main] could not decode json: {e}")
		raise SystemExit(1)
	except:
		main_logger.error(f"[main] open config and json decode, unexpected error: {sys.exc_info()[0]}")
		raise SystemExit(1)
	
	return documents


def got_work():
	if path_has_files(env_config("DISTRIBUTING_INPUT_PATH"), main_logger):
		return True
	else:
		main_logger.info(f"[DISTRIBUTING_files] no files to copy, ending script")
		return False


def distribute_files(documents):
	files_to_distribute = []
	for x in pathlib.Path(env_config("DISTRIBUTING_INPUT_PATH")).iterdir():
		if x.is_file():
			if x.name == "timestamp.txt":
				ct = datetime.now()
				now_timestamp = ct.timestamp()
				timestamp_from_file = 0
				# use file timestamp
				with open(x, "r") as timestamp_file:
					timestamp_from_file = float(timestamp_file.read())

				time_delta = now_timestamp - timestamp_from_file
				time_delta = time_delta / 60
				time_limit = int(env_config("ALARM_TIMELIMIT_MINUTEN"))
				if time_delta > time_limit:
					send_email(timestamp_from_file)
					main_logger.error(f"[distribute_files] time_delta: '{time_delta}' is bigger than: '{time_limit}'; email sent")
			else:
				files_to_distribute.append(x)

	docIds = []
	for document in documents["documents"]:
		docIds.append(document["id"])

	docId_len = int(env_config("DOC_ID_LENGTH"))
	global log_output
	for f in files_to_distribute:
		try:
			if f.name[:docId_len] in docIds:
				main_logger.debug(f"[distribute_files] found file prefix: '{f.name[:docId_len]}' in '{docIds}'")
			else:
				main_logger.error(f"[distribute_files] file prefix '{f.name[:docId_len]}' is not in '{env_config('DISTRIBUTING_CONFIG_PATH')}'")
				continue
		except:
			main_logger.error(f"[distribute_files] unexpected docId_len error: {sys.exc_info()[0]}")

		# config in current document from documents
		document = documents["documents"][docIds.index(f.name[:docId_len])]
		base_path = documents["GWBasePaths"][document["GWBasePath"]]
		new_file_name = f.name[-(len(f.name) - docId_len):]

		destination = join_str_path(base_path, document["GWDestName"])
		destination = join_str_path(destination, new_file_name)

		was_transformation_successful = False
		try:
			if len(document["xsl_path"]):
				dom = ET.parse(str(f))
				for transformation in document["xsl_path"]:
					xslt = ET.parse(transformation)
					transform = ET.XSLT(xslt)
					dom = transform(dom)
				new_file = (ET.tostring(dom, pretty_print=True)).decode("utf-8")
				with open(f, "w") as current_file:
					current_file.write(new_file)
					current_file.close()
					was_transformation_successful = True
			else:
				print("no transform")
				was_transformation_successful = True
		except:
			main_logger.error(f"[distribute_files] failed to transform")

		if not was_transformation_successful:
			main_logger.error(f"[distribute_files] file '{f}' was not transformed, skipping")
			continue

		try:
			shutil.copy(f, destination)
		except:
			main_logger.error(f"[distribute_files] failed to copy: '{f}' to: '{destination}'")
			main_logger.error(f"[distribute_files] Unexpected copy error: " + sys.exc_info()[0])
			continue
		else:
			log_output = f"distribute;{str(f)};{str(destination)}"
			write_stats(log_output)
			main_logger.debug(f"[distribute_files] copied from: {f} to: {destination}")
			# remove the file
			try:
				f.unlink()
			except PermissionError as p:
				main_logger.error(f"[distribute_files] failed to remove: '{f}' error: '{p}'")
			except:
				main_logger.error(f"[distribute_files] Unexpected remove error: " + sys.exc_info()[0])

def main():
	start_time = time.time()
	main_logger.debug(f"[main] code execution started")
	global log_output
	
	if not check_config():
		main_logger.error(f"[main] check config failed!")
		raise SystemExit(1)

	if not check_env():
		main_logger.error(f"[main] check env failed!")
		raise SystemExit(1)
		
	if not get_sftp_files():
		main_logger.error(f"[main] get_sftp_files failed!")
		# raise SystemExit(1)

	documents = get_documents()
	if got_work:
		distribute_files(documents)
	
	stop_time = time.time()
	execution_time = stop_time - start_time
	main_logger.debug(f"[main] finished func in {int(execution_time)} seconds")


'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
log_output = ""
main_logger = common.init_logger("main_logger", env_config("LOG_LOCATION_DISTRIBUTING"), int(env_config("LOG_LEVEL")))
main()
